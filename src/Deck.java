import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

/**
 * Represents a standard 52-card deck, with cards organized into 4 suits and 13 ranks
 */
public class Deck {
    public enum Suit {Clubs, Diamonds, Hearts, Spades};
    public enum Rank {Ace, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King};


    private ArrayList<Card> cardDeck;

    /**
     * Builds a new deck, with cards ordered by suit and rank
     */
    public Deck() {
        cardDeck = new ArrayList<Card>();
        refill();
    }

    /**
     * Discards all remaining cards in the deck and builds a new unshuffled deck.
     * Functionally equivallent to instantiating a new Deck object.
     *
     * @return a reference to itself
     */
    public Deck refill() {
        cardDeck.clear();
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                cardDeck.add(new Card(suit, rank));
            }
        }
        return this;
    }

    /**
     * Removes the top card from the deck and returns it, shrinking the deck by 1 card.
     * Can be called repeatedly to deal out all cards in the deck
     *
     * @return the top card, or null if the deck is empty
     */
    public Card dealOne() {
        if (cardDeck.isEmpty())
            return null;
        else
            return cardDeck.remove(0);
    }

    /**
     * Shuffles the deck in-place and returns it
     *
     * @return a reference to itself
     */
    public Deck shuffle() {
        Collections.shuffle(cardDeck);
        return this;
    }

    /**
     * Represents a single playing card with a specific suit and rank.
     * Once created, a card's suit and rank cannot be changed
     */
    public class Card {
        private Suit suit;
        private Rank rank;

        /**
         * Creates a new instance of Card with the desired suit and rank
         *
         * @param suit the suit of this card
         * @param rank the rank of this card
         */
        public Card(Suit suit, Rank rank) {
            this.suit = suit;
            this.rank = rank;
        }

        /**
         *
         * @return this card's suit
         */
        public Suit getSuit() {
            return suit;
        }

        /**
         *
         * @return this card's rank
         */
        public Rank getRank() {
            return rank;
        }

        @Override
        public String toString() {
            return rank.name() + " of " + suit.name();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Card card = (Card) o;
            return suit == card.suit &&
                    rank == card.rank;
        }

        @Override
        public int hashCode() {
            return Objects.hash(suit, rank);
        }
    }
}
