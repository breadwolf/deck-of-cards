import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DeckTest {

    public Deck deck;

    @Before
    public void setup() {
        deck = new Deck();
    }

    @Test
    public void dealOne_reducesDeck() {
        for (int cardsDealt = 1; cardsDealt < 53; cardsDealt++) {
            assertNotNull("Card number " + cardsDealt + " should exist", deck.dealOne());
        }

        assertNull("A deck should not have a 53rd card", deck.dealOne());
    }

    @Test
    public void newDeck_isOrdered() {
        for (Deck.Suit suit : Deck.Suit.values()) {
            for (Deck.Rank rank : Deck.Rank.values()) {
                Deck.Card card = deck.dealOne();
                assertEquals(suit, card.getSuit());
                assertEquals(rank, card.getRank());
            }
        }
    }

    @Test
    public void refill_isOrdered() {
        deck.dealOne();
        deck.shuffle();
        deck.refill();

        for (Deck.Suit suit : Deck.Suit.values()) {
            for (Deck.Rank rank : Deck.Rank.values()) {
                Deck.Card card = deck.dealOne();
                assertEquals(suit, card.getSuit());
                assertEquals(rank, card.getRank());
            }
        }
    }

    /**
     * While theoretically two shuffled decks could end up being the same, in practice this should never happen
     * If this test fails despite shuffle appearing to work, it's likely the implementation needs more randomness
     */
    @Test
    public void shuffle_changesTheDeckUnpredictably() {
        Deck deck1 = new Deck().shuffle();
        Deck deck2 = new Deck().shuffle();

        boolean decksNotEqual = false;
        for (int cardsDealt = 1; cardsDealt < 53; cardsDealt++) {
            if (deck1.dealOne() != deck2.dealOne()) {
                decksNotEqual = true;
                break;
            }
        }

        assertTrue("At least one pair of cards should differ between two shuffled decks", decksNotEqual);
    }
}
